package collabthings.service.model;

import javax.xml.bind.annotation.XmlRootElement;

import collabthings.service.App;

@XmlRootElement
public class Hello {

	private String info = "" + App.VERSION;
	private String msg;

	public Hello() {

	}

	public Hello(String string) {
		this.setMsg(string);
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
