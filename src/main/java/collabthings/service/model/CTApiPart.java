package collabthings.service.model;

import org.collabthings.model.CTPart;

import collabthings.service.api.ApiID;

public class CTApiPart {
	private String name;
	private String apiid = ApiID.random();

	public CTApiPart() {
		// empty
	}

	public CTApiPart(CTPart p) {
		setPart(p);
	}

	public void setPart(CTPart p) {
		this.name = p.getName();
	}

	public String getApiid() {
		return apiid;
	}

	public void setApiid(String apiid) {
		this.apiid = apiid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setTo(CTPart p) {
		p.setName(name);
	}

}
