package collabthings.service;

import org.collabthings.core.utils.WLogger;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import collabthings.service.api.Api;

public class CTServiceLauncher {
	private static final int SERVICE_PORT = 14311;
	private Server jettyServer;
	private App app;

	private WLogger logger = WLogger.getLogger(this);
	private int port;

	public CTServiceLauncher(int i) {
		this.port = i;
	}

	public CTServiceLauncher() {
		this.port = CTServiceLauncher.SERVICE_PORT;
	}

	public void init() throws Exception {
		app = App.getInstance();
		app.launch();

		Thread.sleep(2000);

		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
		context.setContextPath(App.BASE_URI);

		ResourceHandler statichandler = new ResourceHandler();
		statichandler.setDirectoriesListed(true);
		statichandler.setResourceBase("static");

		HandlerList list = new HandlerList();
		list.addHandler(context);
		list.addHandler(statichandler);

		jettyServer = new Server(port);
		jettyServer.setHandler(list);

		ServletHolder jerseyServlet = context.addServlet(org.glassfish.jersey.servlet.ServletContainer.class, "/*");

		// Tells the Jersey Servlet which REST service/class to load.
		String packageName = Api.class.getPackageName();
		WLogger.getLogger(this).info("package " + packageName);
		jerseyServlet.setInitParameter("jersey.config.server.provider.packages",
				packageName + ", com.vogella.jersey.jaxb");

		jettyServer.start();
	}

	public static void main(String[] args) throws Exception {
		CTServiceLauncher m = new CTServiceLauncher();
		m.init();

	}

	public void stop() {
		app.close();
		try {
			jettyServer.stop();
		} catch (Exception e) {
			logger.error(e);
		}
	}

	public String getApiUrl() {
		return "http://localhost:" + port + App.BASE_URI;
	}
}
