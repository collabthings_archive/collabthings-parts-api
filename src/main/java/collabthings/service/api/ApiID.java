package collabthings.service.api;

import org.collabthings.datamodel.WStringID;

public class ApiID {

	public static String random() {
		return new WStringID().toString();
	}

}
