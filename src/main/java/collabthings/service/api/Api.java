package collabthings.service.api;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.collabthings.app.CTApp;
import org.collabthings.model.CTPart;

import collabthings.service.App;
import collabthings.service.model.CTApiPart;
import collabthings.service.model.Hello;

@Path("/")
public class Api {
	private App app = App.getInstance();
	private CTApp ctapp = app.getApp();

	@GET
	@Path("test")
	@Produces({ MediaType.APPLICATION_JSON })
	public Hello Testt() {
		return new Hello("Hello");
	}

	@GET
	@Path("newpart")
	@Produces({ MediaType.APPLICATION_JSON })
	public CTApiPart newPart() {
		CTPart p = ctapp.newPart();
		CTApiPart apipart = new CTApiPart();
		apipart.setPart(p);

		app.setPart(apipart.getApiid(), p);

		return apipart;
	}

	@POST
	@Path("savepart")
	public void savePart(CTApiPart part) {
		CTPart p = app.getPart(part.getApiid());
		part.setTo(p);
	}

	@GET
	@Path("getpart/{apiid}")
	@Produces({ MediaType.APPLICATION_JSON })
	public CTApiPart getPart(@PathParam("apiid") String apiid) {
		CTPart p = app.getPart(apiid);
		return new CTApiPart(p);
	}

}
