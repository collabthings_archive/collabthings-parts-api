package collabthings.service;

import static org.junit.Assert.assertEquals;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.collabthings.core.utils.WLogger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import collabthings.service.model.CTApiPart;

public class ApiTest {

	private WebTarget target;
	private CTServiceLauncher ctservice;
	private WLogger logger = WLogger.getLogger(this);

	@Before
	public void setUp() throws Exception {
		// start the server
		ctservice = new CTServiceLauncher(10111);
		ctservice.init();
		// create the client
		Client c = ClientBuilder.newClient();

		target = c.target(ctservice.getApiUrl());
	}

	@After
	public void tearDown() throws Exception {
		ctservice.stop();
	}

	@Test
	public void testHello() {
		WebTarget path = target.path("test");
		logger.info("path " + path);
		String responseMsg = path.request().get(String.class);
		assertEquals("{\"info\":\"0.1\",\"msg\":\"Hello\"}", responseMsg);
	}

	@Test
	public void testPart() {
		CTApiPart part = target.path("newpart").request().get(CTApiPart.class);
		assertEquals("part", part.getName());

		part.setName("testname" + System.currentTimeMillis());
		target.path("savepart").request().post(Entity.entity(part, MediaType.APPLICATION_JSON), CTApiPart.class);

		CTApiPart partagain = target.path("getpart").path(part.getApiid()).request().get(CTApiPart.class);
		assertEquals(part.getName(), partagain.getName());
	}
}
