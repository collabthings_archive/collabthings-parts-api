#!/bin/bash

jarfile=$(find service -name *.jar)
echo jar found ${jarfile}

md5sum ${jarfile}

while [ 1 ]; do
	sum_a=$(md5sum ${jarfile})
	echo Sum A ${sum_a}
	java -classpath ${jarfile}:service/target/classes	collabthings/service/CTServiceLauncher &

	while [ "${sum_a}" = "$(md5sum ${jarfile})" ]; do
		echo .
		sleep 1
	done
	
	ps aux | grep java | grep CTSer | awk -S '{ print $2; };' | xargs --verbose kill
	
	sleep 2
done

